# qmcmary.jl

## Roadmap

### 功能
- [x] 完成dqmc的流程
- [x] 优化一个小格点看效果；能否优化常规的一维链或者hexagonal
- [x] 对Hamiltonian中的参数求导
- [ ] 整理adso.jl中的接口参数
- [ ] 增加source term


### 后续
- [ ] 支持虚数U的HS变换
- [ ] 优化前向计算时候的性能
- [ ] 优化参数名，ehk和hk区分好
- [ ] dt在什么地方设置？
- [ ] Adam？dropout？
- [ ] 完善非等时格林函数
- [ ] 是否需要重新实现一个svd，继续改善svd的稳定问题
- [ ] 重新整理计算流程。


## Notes
- 区分D^l和D^s对结果有比较大的改善，之前的一些问题都是这个原因包括：
    - SVD的时候，如果全在L会有数值不正确的问题
    - 验证gbar的时候两端（全在L或者全在R）的时候数值不稳定

- d log(S) = d \sum w - d \sum log(abs(w))，优化的是后者，前者可能会减小从而导致平均符号更差，因为抽样的时候抽的不全，前者的数值并不是常数。

- thetabar_test用来验证结果是否回归（如果要增加test中的依赖，先activate ./test环境）

- 在计算导数的时候，在n_{x,y,z}更新前，dB/dn_{x,y,z}不需要每次都算，只需要根据HS场拼凑出就行了，每个tau只有辅助场上或者下两种情况，不需要调用ForwardDiff，只需要用以前的去累加就行了。

- 在利用mpi的时候，不能确定openmpi+openblas的组合会不会出问题。intelmpi+openblas不会出问题，但是性能会有问题（可能是openblas的多线程实现效率不行，可以试试设置BLAS.set_num_threads）。intelmpi+MKL sequential性能足够。using MKL要放在最上面。

- 在对mu或者tp等参数求导时，注意我们需要的是加上这项后B矩阵的变化。所以需要先在ehk中去掉，然后再求B矩阵的函数里再乘回来。这样才能求出对应数值中的导数。原先的实现中，引入了baresp，即没有mu的ehk，然后通过B=eV\*emu\*ehk来对mu进行自动求导，这点成立的前提是mu不会相互影响。在计算tp导数的过程中，tp会相互影响。所以以后就不用baresp，而是在计算前乘一个逆矩阵去掉tp或者mu。

## Reference

1. Mitigating the fermion sign problem by automatic differentiation. Zhou-Quan Wan, Shi Xin Zhang, and Hong Yao. Phys. Rev. B 106, L241109