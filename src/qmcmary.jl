module qmcmary


using LinearAlgebra

include("core/hstrans.jl")
export fakedata, bmat_Ising, Δmat_Ising, bmat_IsingAD, bmat_IsingND, Δmat_IsingND
export bmat_IsingADX, bmat_IsingADTX
export bmat_IsingZMuAD, bmat_IsingZMuND
export bmat_Gauge1ADX, bmat_Gauge1AD, bmat_Gauge1ND, Δmat_Gauge1ND

include("core/scrollstable.jl")
export ScrollSVD, ScrollSVDZ, ScrollIter, scrollL2R, scrollR2L

include("core/splitting.jl")
export Splitting, default_splitting, unpack_splitting
export default_splitting2, default_splitting3

include("core/syntaxmisc.jl")
export @✓, @←, @↻, @↺

include("core/mechlrn.jl")
export @autojac, @autojac2, Adam, next, AutoJacTape

include("common.jl")
export lattice_chain, lattice_hexagonal, lattice_kagome, lattice_ionic_square, lattice_ionic_chain
export lattice_tprim_square, lattice_bilayer_square

include("adso.jl")
export pbpn_calc, pbpn_calc_xi, bbar_calc, bbar_from_gbar, meas_grad, gbar_scratch, down_prog_gbar
export pbpn_calc_tapemap
export meas_gradmu, pbpmu_calc_tapemap, pbpmu_calc_xi
export meas_gradU, pbpU_calc_tapemap, pbpU_calc_xi
export meas_gradtp, pbptp_calc_tapemap, pbptp_calc_xi
export pbpa_calc_tapemap, pbpa_calc_xi, meas_grada

include("dqmc.jl")
export eq_green_scratch, down_prog_green, up_prog_green, qmcstatus, Woodbury, PressShermanMorrison!
export ueq_green_scratch
export initialize_SS, dqmc_step
export ueq_green_func
export initialize_SS_Gauge1, dqmc_step_Gauge1


end # module qmcmary
