#=
切分的功能
=#


struct Splitting{T,P}
    segl    ::Int
    vecl    ::Vector{Int}
    Uit     ::Matrix{P}
    sizehk  ::Tuple{Int, Int}
    ehkt    ::Matrix{T}
    dt      ::Float64
end


"""
获得参数
"""
function unpack_splitting(sp::Splitting, bi)
    Ui = sp.Uit[sp.vecl[bi], :]
    ehk = sp.ehkt[sp.vecl[bi], :]
    return reshape(ehk, sp.sizehk), Ui
end


#"""
#获得参数
#"""
#function Base.getindex(sp::Splitting, ni::Int)
#    Ui = sp.Uit[sp.vecl[ni], :]
#    ehk = sp.ehkt[sp.vecl[ni], :]
#    return reshape(ehk, sp.sizehk), Ui
#end


"""
默认的切分
"""
function default_splitting(Nt::Int, hk::Matrix{T}, Ui::Vector{P};
    Z2=true, dt=0.1) where {T, P}
    Uit = zeros(P, 1, length(Ui))
    Uit[1, :] = Ui
    #
    ehk2 = exp(-dt*hk)
    if Z2
        ehk2 = kron([1 0; 0 1], ehk2)
    else
        ehk2 = ehk2
    end
    ehk_ = vec(ehk2)
    ehkt = zeros(T, 1, length(ehk_))
    ehkt[1, :] = ehk_
    return Splitting(
        1,
        ones(Int, Nt),
        Uit,
        size(ehk2),
        ehkt,
        dt
    )
end


"""
默认的切分
"""
function default_splitting2(Nt::Int, hk::Matrix{T}, Ui::Vector{P};
    Z2=true, dt=0.1) where {T, P}
    Uit = zeros(P, 2, length(Ui))
    Uit[1, :] = Ui
    Uit[2, :] = Ui
    #
    ehk1 = exp(-(dt+0.1im)*hk)
    ehk2 = exp(-(dt-0.1im)*hk)
    if Z2
        ehk1 = kron([1 0; 0 1], ehk1)
        ehk2 = kron([1 0; 0 1], ehk2)
    else
        ehk1 = ehk1
        ehk2 = ehk2
    end
    ehk1_ = vec(ehk1)
    ehkt = zeros(T, 2, length(ehk1_))
    ehkt[1, :] = ehk1_
    ehkt[2, :] = vec(ehk2)
    #
    return Splitting(
        2,
        [Int(1 + (bi+1)%2) for bi in Base.OneTo(Nt)],
        Uit,
        size(ehk2),
        ehkt,
        dt
    )
end


"""
默认的切分
"""
function default_splitting3(Nt::Int, hk::Matrix{T}, Ui::Vector{P};
    Z2=true, dt=0.1) where {T, P}
    Uit = zeros(P, 2, length(Ui))
    Uit[1, :] = 2*Ui
    Uit[2, :] = zeros(P, length(Ui))
    #
    ehk1 = exp(-complex(dt, dt)*hk)
    ehk2 = exp(-complex(dt, -dt)*hk)
    if Z2
        ehk1 = kron([1 0; 0 1], ehk1)
        ehk2 = kron([1 0; 0 1], ehk2)
    else
        ehk1 = ehk1
        ehk2 = ehk2
    end
    ehk1_ = vec(ehk1)
    ehkt = zeros(T, 2, length(ehk1_))
    ehkt[1, :] = ehk1_
    ehkt[2, :] = vec(ehk2)
    #
    return Splitting(
        2,
        [Int(1 + (bi+1)%2) for bi in Base.OneTo(Nt)],
        Uit,
        size(ehk2),
        ehkt,
        dt
    )
end



