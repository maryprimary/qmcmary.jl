#=
杂项
=#

macro ✓(expr)
    esc(expr)
end

macro ✓(expr::Expr)
    if expr.head !== :(=)
        return esc(expr)
    end
    lhs = expr.args[1]
    rhs = expr.args[2]
    if isa(lhs, Expr) && lhs.head === :tuple
        largs = lhs.args
        rargs = rhs.args
        #exp1 = [Expr(:(=), lr[1], lr[2]) for lr in zip(lhs.args, rhs.args)]
        return esc(quote
            for lr in zip($lhs, $rhs)
                if !all(isapprox.(lr[1], lr[2], atol=9e-1))
                    @warn "!all(isapprox.(lr[1], lr[2], atol=9e-1))"
                end
            end
            #for exp in $exp1
            #    exp
            #end
            $expr
        end)
    elseif isa(lhs, Symbol)
        esc(quote
            if !all(isapprox.($lhs, $rhs, atol=9e-1))
                @warn "!all(isapprox.(..., atol=9e-1))"
            end
            $lhs = $rhs
        end)
    else
        return esc(expr)
    end
end


"""
比较之后替代
"""
macro ←(a, b)
    esc(quote
        if !all(isapprox.($a, $b, atol=9e-1))
            @warn "!all(isapprox.(..., atol=9e-1))"
        end
        $a = $b
    end)
end


"""
增加时调用
"""
macro ↻(L, s)
    quote
        local s1 = $(esc(s))
        while s1 > $(esc(L))
            s1 = s1-$(esc(L))
        end
        s1
    end
end


"""
减去时调用
"""
macro ↺(L, s)
    quote
        local s1 = $(esc(s))
        while s1 < 1
            s1 = s1+$(esc(L))
        end
        s1
    end
end

