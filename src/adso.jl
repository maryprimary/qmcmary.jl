#=
计算导数
=#

#using ReverseDiff: jacobian!, JacobianTape
#using Zygote
using ForwardDiff: jacobian, derivative


function gbar_scratch(ss::ScrollSVD{T}) where T
    siz = size(ss.B[end])
    ptr = findfirst(ss.L)
    if isnothing(ptr)
        VL = Diagonal(ones(siz[1]))
        DL = VL
        UL = VL
        UR, DR, VR = ss.F[end].U, Diagonal(ss.F[end].S), ss.F[end].Vt
        #VL, DL, UL = ss.F[end].U, Diagonal(ss.F[end].S), ss.F[end].Vt
        #UR = Diagonal(ones(siz[1]))
        #DR = UR
        #VR = UR
    elseif ptr == 1
        VL, DL, UL = ss.F[ptr].U, Diagonal(ss.F[ptr].S), ss.F[ptr].Vt
        UR = Diagonal(ones(siz[1]))
        DR = UR
        VR = UR
        #VL = Diagonal(ones(siz[1]))
        #DL = VL
        #UL = VL
        #UR, DR, VR = ss.F[1].U, Diagonal(ss.F[1].S), ss.F[1].Vt
    else
        VL, DL, UL = ss.F[ptr].U, Diagonal(ss.F[ptr].S), ss.F[ptr].Vt
        UR, DR, VR = ss.F[ptr-1].U, Diagonal(ss.F[ptr-1].S), ss.F[ptr-1].Vt
    end
    #gtt = inv(Diagonal(ones(siz[1]))+UR*DR*VR*VL*DL*UL)
    #M = inv(UL*UR) + DR*(VR*VL)*DL
    #Fm = svd(M)
    #gtt = inv(Fm.Vt*UL)*inv(Diagonal(Fm.S))*inv(UR*Fm.U)
    #
    #println(diag(DL))
    #println(diag(DR))
    #M = UL*UR + inv(DL)*adjoint(VR*VL)*inv(DR)
    DLS = Diagonal(ones(Float64, siz[1]))
    DLB = Diagonal(ones(Float64, siz[1]))
    DRS = Diagonal(ones(Float64, siz[1]))
    DRB = Diagonal(ones(Float64, siz[1]))
    for i in Base.OneTo(siz[1])
        if DL[i, i] > 1.0
            DLB[i, i] = DL[i, i]
            DLS[i, i] = 1.0
        else
            DLS[i, i] = DL[i, i]
            DLB[i, i] = 1.0
        end
        if DR[i, i] > 1.0
            DRB[i, i] = DR[i, i]
            DRS[i, i] = 1.0
        else
            DRS[i, i] = DR[i, i]
            DRB[i, i] = 1.0
        end
    end
    #
    M = DLS*UL*UR*DRS + inv(DLB)*adjoint(VR*VL)*inv(DRB)
    #M = DL*UL*UR + adjoint(VR*VL)*inv(DR)
    Fm = svd(M, alg=LinearAlgebra.QRIteration())
    ML = UR*DRS*adjoint(Fm.Vt)
    MR = adjoint(Fm.U)*DLS*UL
    btt = ML*inv(Diagonal(Fm.S))*MR
    #println(Fm.S)
    #btt = UR*adjoint(Fm.Vt)*inv(Diagonal(Fm.S))*adjoint(Fm.U)*UL
    #btt = adjoint(Fm.Vt*adjoint(UR))*inv(Diagonal(Fm.S))*adjoint(adjoint(UL)*Fm.U)
    #btt = UR*DR*adjoint(Fm.Vt)*inv(Diagonal(Fm.S))*adjoint(adjoint(UL)*Fm.U)
    #btt = UR*DR*adjoint(Fm.Vt)*inv(Diagonal(Fm.S))*adjoint(Fm.U)*DL*UL
    #M = DL*UL*UR + adjoint(VR*VL)*inv(DR)
    #Fm = svd(M, alg=LinearAlgebra.QRIteration())
    ##println(Fm.S)
    ##btt = UR*adjoint(Fm.Vt)*inv(Diagonal(Fm.S))*adjoint(Fm.U)*UL
    ##btt = adjoint(Fm.Vt*adjoint(UR))*inv(Diagonal(Fm.S))*adjoint(adjoint(UL)*Fm.U)
    #btt = adjoint(Fm.Vt*adjoint(UR))*inv(Diagonal(Fm.S))*adjoint(Fm.U)*DL*UL
    return btt
end


"""
向下传递一个
"""
function down_prog_gbar(bbar::Matrix{T}, bmat::Matrix{T}) where T
    return inv(bmat)*bbar*bmat
end


"""
从 \\bar{B} = - [ \\bar{G} B^-1 ]^T
"""
function bbar_from_gbar(gbar::Matrix{T}, bmat::Matrix{T}) where T
    return -transpose(gbar * inv(bmat))
end


"""
计算B矩阵的导数，包含数值稳定的办法
"""
function bbar_calc(ss::ScrollSVD{T}, allbmats1::Vector{Matrix{T}}) where T
    Nt = length(allbmats1)
    sslen = length(ss.L)
    Ng = Int(Nt//sslen)
    xsiz = size(allbmats1[end])
    Nx = Int(xsiz[1]//2)
    # 求 \bar  B_tau_xy
    #bbar = Array{ComplexF64, 3}(undef, Nt, 2*Nx, 2*Nx)
    #for bi in Base.OneTo(Nt)
    #    gs1 = rawgbar(allbmats1, bi-1)
    #    bba = bbar_from_gbar(gs1, allbmats1[bi])
    #    bbar[bi, :, :] = bba
    #end
    #在观测的阶段所有，ss中所有内容都应在R中
    if any(ss.L)
        throw(error("has L"))
    end
    #计算gs的顺序是1，N, N-1, , N-Ng+2, 然后推一个R进入L
    bbar2 = Array{ComplexF64, 3}(undef, Nt, 2*Nx, 2*Nx)
    for grpi in Base.OneTo(sslen)
        gss = gbar_scratch(ss)
        for ni in Base.OneTo(Ng)
            gi = Nt - Ng*(grpi-1) - ni + 1
            bi = gi+1 > Nt ? 1 : gi+1
            bbar2[bi, :, :] = bbar_from_gbar(gss, allbmats1[bi])
            #println(grpi, " ", gi)
            gss = down_prog_gbar(gss, allbmats1[gi])
        end
        scrollR2L(ss, [ss.B[sslen-grpi+1]])
        #println(sslen-grpi+1, " ", ss.L)
    end
    #for bi in Base.OneTo(Nt)
    #    println(sum(abs.(bbar[bi, :, :]-bbar2[bi, :, :])))
    #end
    #然后将所有的内容返回R
    for grpi in Base.OneTo(sslen)
        scrollL2R(ss, [ss.B[grpi]])
        #println(grpi, " ", ss.L)
    end
    return bbar2
end

#=
"""
计算B矩阵的正向导数
"""
function pbpn_calc_(hk, hscfg::Matrix{Int}, U::Float64, nx::Vector{Float64}, ny::Vector{Float64}, nz::Vector{Float64}; Z2=true)
    Nt, Nx = size(hscfg)
    #hscfg_f64 = Float64.(hscfg)
    #ReverseDiff
    #fbr(cfg, ny, nz) = bmat_IsingAD(ehk, cfg, U*ones(Nx), zeros(Nx), ny, nz; Z2=Z2)[1]
    #fbi(cfg, ny, nz) = bmat_IsingAD(ehk, cfg, U*ones(Nx), zeros(Nx), ny, nz; Z2=Z2)[2]
    #jfbr = JacobianTape(fbr, (rand(Float64, Nx), rand(Float64, Nx), rand(Float64, Nx)))
    #jfbi = JacobianTape(fbi, (rand(Float64, Nx), rand(Float64, Nx), rand(Float64, Nx)))
    #undef会有奇怪的问题，以后排查一下
    #这里内存用的太多了，优化成随bi和xi走的，节约内存
    #pbrpny = zeros(Nt, 2*Nx, 2*Nx, Nx)#Array{Float64, 4}(undef, Nt, 2*Nx, 2*Nx, Nx)
    #pbipny = zeros(Nt, 2*Nx, 2*Nx, Nx)#Array{Float64, 4}(undef, Nt, 2*Nx, 2*Nx, Nx)
    #pbrpnz = zeros(Nt, 2*Nx, 2*Nx, Nx)#Array{Float64, 4}(undef, Nt, 2*Nx, 2*Nx, Nx)
    #pbipnz = zeros(Nt, 2*Nx, 2*Nx, Nx)#Array{Float64, 4}(undef, Nt, 2*Nx, 2*Nx, Nx)
    #
    dt = 0.1
    ehk_ = exp(-dt*hk)
    if Z2
        ehk2 = kron([1 0; 0 1], ehk_)
    else
        ehk2 = ehk_
    end
    #fbrxp1(ehk, ny, nz) = bmat_IsingADX(ehk, Val(1), U, 0.0, ny, nz)[1]
    #fbrxm1(ehk, ny, nz) = bmat_IsingADX(ehk, Val(-1), U, 0.0, ny, nz)[1]
    #
    #@time begin
    #for bi in Base.OneTo(Nt)
    #    #println("pbpn ", bi)
    #    #result = (zeros(Float64, 2*Nx, 2*Nx, Nx), zeros(Float64, 2*Nx, 2*Nx, Nx), zeros(Float64, 2*Nx, 2*Nx, Nx))
    #    #jacobian!(result, jfbr, (hscfg_f64[bi, :], ny, nz))
    #    #_, brny, brnz = result
    #    #@time begin
    #    #_, brny, brnz = Zygote.jacobian(fbr, hscfg_f64[bi, :], ny, nz)
    #    #_, biny, binz = Zygote.jacobian(fbi, hscfg_f64[bi, :], ny, nz)
    #    #end
    #    #b1 = reshape(brny, 2*Nx, 2*Nx, Nx)
    #    #b2 = reshape(brnz, 2*Nx, 2*Nx, Nx)
    #    #b3 = reshape(biny, 2*Nx, 2*Nx, Nx)
    #    #b4 = reshape(binz, 2*Nx, 2*Nx, Nx)
    #    #println(b1[1, 1:3, 1], b1[2, 1:3, 1], b1[19, 1:3, 1])
    #    #@time begin
    #    for xi in Base.OneTo(Nx)
    #        ehkslice = ehk2[[xi, xi+Nx], :]
    #        #前向
    #        funcforward = (n) -> bmat_IsingADX(ehkslice, Val(hscfg[bi, xi]), U, 0.0, n[1], n[2])
    #        jacxi = jacobian(funcforward, [ny[xi], nz[xi]])
    #        #这边不同tau时候的操作其实是相同的，应该可以
    #        #[11(hscfg[1,xi],n) 12(hscfg[1,xi],n); 11(hscfg[tau,xi],n) 12(hscfg[tau,xi],n)]
    #        #1. [M11(1) M12(1); ...; M11(tau) M12(tau)] * ehkslice -> (tau, 2*Nx)
    #        #2. [M21(1) M22(1); ...; M21(tau) M22(tau)] * ehkslice -> (tau, 2*Nx)
    #        #reshape(1., tau, 1, 2*Nx) ...
    #        #cat(real1, real2, imag1, imag2; dims=2) -> (tau, 4, 2*Nx)
    #        #diff -> (tau, 4, 2*Nx, 2)
    #        #@time jacxi = @autojac(ehkslice, hscfg, bi, xi, U, ny, nz)
    #        #println("_")
    #        #@time jacxi_ = @autojac2(ehkslice, hscfg, bi, xi, U, ny, nz)
    #        #println(size(jacxi), size(jacxi_))
    #        #@assert all(isapprox.(jacxi, jacxi_))
    #        jacxi = reshape(jacxi, 4, 2*Nx, 2)
    #        pbrpny[bi, [xi, xi+Nx], :, xi] = jacxi[1:2, :, 1]
    #        pbrpnz[bi, [xi, xi+Nx], :, xi] = jacxi[1:2, :, 2]
    #        pbipny[bi, [xi, xi+Nx], :, xi] = jacxi[3:4, :, 1]
    #        pbipnz[bi, [xi, xi+Nx], :, xi] = jacxi[3:4, :, 2]
    #    end
    #    #end
    #    #println(pbrpny[bi, :, :, :], b1)
    #    #println(pbrpnz[bi, :, :, :], b2)
    #    #println(maximum(pbrpny[bi, :, :, :] - b1), maximum(pbrpnz[bi, :, :, :] - b2))
    #    #@assert all(isapprox.(pbrpny[bi, :, :, :], b1))
    #    #@assert all(isapprox.(pbrpnz[bi, :, :, :], b2))
    #    #println(maximum(pbipny[bi, :, :, :] - b3), maximum(pbipnz[bi, :, :, :] - b4))
    #    #@assert all(isapprox.(pbipny[bi, :, :, :], b3))
    #    #@assert all(isapprox.(pbipnz[bi, :, :, :], b4))
    #    #pbrpny[bi, :, :, :] = reshape(brny, 2*Nx, 2*Nx, Nx)
    #    #pbrpnz[bi, :, :, :] = reshape(brnz, 2*Nx, 2*Nx, Nx)
    #    #
    #    #@time jacobian!(result, jfbi, (hscfg_f64[bi, :], ny, nz))
    #    #_, biny, binz = result
    #    #b1 = biny
    #    #@time _, biny, binz = jacobian(fbi, hscfg_f64[bi, :], ny, nz)
    #    #b2 = biny
    #    #println(size(b1), size(b2))
    #    #@assert all(isapprox.(reshape(b1, 2*Nx, 2*Nx, Nx), reshape(b2, 2*Nx, 2*Nx, Nx), atol=1e-13))
    #    #@time begin
    #    #for xi in Base.OneTo(Nx)
    #    #    ehkslice = ehk2[[xi, xi+Nx], :]
    #    #    #前向
    #    #    funcforward = (n) -> bmat_IsingADX(ehkslice, Val(hscfg[bi, xi]), U, 0.0, n[1], n[2])[2]
    #    #    jacxi = jacobian(funcforward, [ny[xi], nz[xi]])
    #    #    pbipny[bi, [xi, xi+Nx], :, xi] = reshape(jacxi[:, 1], 2, 2*Nx)
    #    #    pbipnz[bi, [xi, xi+Nx], :, xi] = reshape(jacxi[:, 2], 2, 2*Nx)
    #    #end
    #    #end
    #    #pbipny[bi, :, :, :] = reshape(biny, 2*Nx, 2*Nx, Nx)
    #    #pbipnz[bi, :, :, :] = reshape(binz, 2*Nx, 2*Nx, Nx)
    #    #
    #end
    #end
    #println("tau")
    #这里内存用的太多了，优化成随bi和xi走的，节约内存
    pbrpnx2 = zeros(Nt, 2*Nx, 2*Nx, Nx)#Array{Float64, 4}(undef, Nt, 2*Nx, 2*Nx, Nx)
    pbipnx2 = zeros(Nt, 2*Nx, 2*Nx, Nx)
    pbrpny2 = zeros(Nt, 2*Nx, 2*Nx, Nx)#Array{Float64, 4}(undef, Nt, 2*Nx, 2*Nx, Nx)
    pbipny2 = zeros(Nt, 2*Nx, 2*Nx, Nx)#Array{Float64, 4}(undef, Nt, 2*Nx, 2*Nx, Nx)
    pbrpnz2 = zeros(Nt, 2*Nx, 2*Nx, Nx)#Array{Float64, 4}(undef, Nt, 2*Nx, 2*Nx, Nx)
    pbipnz2 = zeros(Nt, 2*Nx, 2*Nx, Nx)
    #@time begin
    for xi in Base.OneTo(Nx)
        ehkslice = ehk2[[xi, xi+Nx], :]
        funcforward2 = (n) -> bmat_IsingADTX(ehkslice, hscfg[:, xi], U, n[1], n[2], n[3])
        jacxit = jacobian(funcforward2, [nx[xi], ny[xi], nz[xi]])
        jacxit = reshape(jacxit, 4*Nt, 2*Nx, 3)
        #funcforward3 = (ehk, ths, n) ->  bmat_IsingADTX(ehk, ths, U, 0.0, n[1], n[2])
        #println("Enzyme")
        #@time jacxit2 = cat(autodiff(Forward, funcforward3, BatchDuplicated, Const(ehkslice), Const(hscfg[:, xi]),
        #BatchDuplicated([ny[xi], nz[xi]], ([1.0, 0.0], [0.0, 1.0])))[2]...; 
        #dims=3)
        #println(size(jacxit2))
        #@assert all(isapprox.(jacxit, jacxit2))
        pbrpnx2[:, xi, :, xi]    = jacxit[1:Nt, :, 1]
        pbrpnx2[:, xi+Nx, :, xi] = jacxit[(Nt+1):2Nt, :, 1]
        pbrpny2[:, xi, :, xi]    = jacxit[1:Nt, :, 2]
        pbrpny2[:, xi+Nx, :, xi] = jacxit[(Nt+1):2Nt, :, 2]
        pbrpnz2[:, xi, :, xi]    = jacxit[1:Nt, :, 3]
        pbrpnz2[:, xi+Nx, :, xi] = jacxit[(Nt+1):2Nt, :, 3]
        #
        pbipnx2[:, xi, :, xi]    = jacxit[(2Nt+1):3Nt, :, 1]
        pbipnx2[:, xi+Nx, :, xi] = jacxit[(3Nt+1):4Nt, :, 1]
        pbipny2[:, xi, :, xi]    = jacxit[(2Nt+1):3Nt, :, 2]
        pbipny2[:, xi+Nx, :, xi] = jacxit[(3Nt+1):4Nt, :, 2]
        pbipnz2[:, xi, :, xi]    = jacxit[(2Nt+1):3Nt, :, 3]
        pbipnz2[:, xi+Nx, :, xi] = jacxit[(3Nt+1):4Nt, :, 3]
        #println(jacxit[1, 1:2, :, 1], pbrpny[1, [1, 1+Nx], :, 1])
        #@assert all(isapprox.(jacxit[:, 1:2, :, 1], pbrpny[:, [xi, xi+Nx], :, xi]))
        #@assert all(isapprox.(jacxit[:, 1:2, :, 2], pbrpnz[:, [xi, xi+Nx], :, xi]))
        #@assert all(isapprox.(jacxit[:, 3:4, :, 1], pbipny[:, [xi, xi+Nx], :, xi]))
        #@assert all(isapprox.(jacxit[:, 3:4, :, 2], pbipnz[:, [xi, xi+Nx], :, xi]))
    end
    #end
    return pbrpnx2, pbipnx2, pbrpny2, pbipny2, pbrpnz2, pbipnz2
end
=#


"""
计算每个格点上的jactape，在计算∂B(τ)/∂n_{x,y,z;i}的时候，所有的i之间和τ之间没有什么关联，
每个格点和虚时都单看只有辅助场s_{τ,i}=±1的两种，把他们提前算出来，然后在不同HS场下平均的时候就不要重复计算了。
"""
function pbpn_calc_tapemap(sp::Splitting, nx::Vector{Float64}, ny::Vector{Float64}, nz::Vector{Float64})
    #
    Nx = length(nx)
    tapemap = Matrix{AutoJacTape}(undef, Nx, 2*sp.segl)
    for si in Base.OneTo(sp.segl)
        ehk2 = reshape(sp.ehkt[si, :], sp.sizehk)
        Ui = sp.Uit[si, :]
        for xi in Base.OneTo(Nx)
            ehkslice = ehk2[[xi, xi+Nx], :]
            if abs(Ui[xi]) > 1e-6
                funcforward1 = (n) -> bmat_IsingADX(ehkslice, Val(-1), Ui[xi], n[1], n[2], n[3], sp.dt)
                funcforward2 = (n) -> bmat_IsingADX(ehkslice, Val(1), Ui[xi], n[1], n[2], n[3], sp.dt)
                jacxi1 = jacobian(funcforward1, [nx[xi], ny[xi], nz[xi]])
                jacxi2 = jacobian(funcforward2, [nx[xi], ny[xi], nz[xi]])
            else
                jacxi1 = zeros(Float64, 4*2*Nx*3)
                jacxi2 = zeros(Float64, 4*2*Nx*3)
            end
            tapemap[xi, 2*si-1] = AutoJacTape([nx[xi], ny[xi], nz[xi]], jacxi1; ehkslice=ehkslice, val=-1, U=Ui[xi])
            tapemap[xi, 2*si] = AutoJacTape([nx[xi], ny[xi], nz[xi]], jacxi2; ehkslice=ehkslice, val=1, U=Ui[xi])
        end
    end
    return tapemap
end


"""
计算B矩阵的正向导数
"""
function pbpn_calc_xi(sp, hsxi::Vector{Int}, xidx::Int, nxi::Float64, nyi::Float64, nzi::Float64;
    tapemap=missing)
    #
    Nt = length(hsxi)
    Nx = length(sp.Uit[1, :])
    pbrpnx2 = zeros(Nt, 2, 2*Nx)
    pbipnx2 = zeros(Nt, 2, 2*Nx)
    pbrpny2 = zeros(Nt, 2, 2*Nx)
    pbipny2 = zeros(Nt, 2, 2*Nx)
    pbrpnz2 = zeros(Nt, 2, 2*Nx)
    pbipnz2 = zeros(Nt, 2, 2*Nx)
    #funcforward2 = (n) -> bmat_IsingADTX(ehkslice, hsxi, Ui, n[1], n[2], n[3])
    #jacxit = jacobian(funcforward2, [nxi, nyi, nzi])
    #jacxit = reshape(jacxit, 4*Nt, 2*Nx, 3)
    #
    if !ismissing(tapemap)
        for bi in Base.OneTo(Nt)
            #jac1 = vcat(jacxit[[bi, Nt+bi], :, :], jacxit[[2Nt+bi, 3Nt+bi], :, :])
            segl = sp.vecl[bi]
            tape = hsxi[bi] == 1 ? tapemap[xidx, 2*segl] : tapemap[xidx, 2*segl-1]
            jac1 = reshape(tape.output, 4, 2*Nx, 3)
            #@assert all(isapprox.(tape.input, [nxi, nyi, nzi]))
            #@assert all(isapprox.(jac1, reshape(tape.output, 4, 2*Nx, 3)))
            pbrpnx2[bi, :, :] = jac1[1:2, :, 1]
            pbrpny2[bi, :, :] = jac1[1:2, :, 2]
            pbrpnz2[bi, :, :] = jac1[1:2, :, 3]
            pbipnx2[bi, :, :] = jac1[3:4, :, 1]
            pbipny2[bi, :, :] = jac1[3:4, :, 2]
            pbipnz2[bi, :, :] = jac1[3:4, :, 3]
        end
    else
        @warn "计算jacobian"
        #Ui = sp.Uit[:, xidx]
        #Ui = sum(Ui) / length(Ui)
        for bi in Base.OneTo(Nt)
            ehk2, Ui = unpack_splitting(sp, bi)
            funcforward2 = (n) -> bmat_IsingADX(ehk2[[xidx,xidx+Nx], :],
            Val(hsxi[bi]), Ui[xidx], n[1], n[2], n[3], sp.dt)
            jacxit = jacobian(funcforward2, [nxi, nyi, nzi])
            jac1 = reshape(jacxit, 4, 2*Nx, 3)
            pbrpnx2[bi, :, :] = jac1[1:2, :, 1]
            pbrpny2[bi, :, :] = jac1[1:2, :, 2]
            pbrpnz2[bi, :, :] = jac1[1:2, :, 3]
            pbipnx2[bi, :, :] = jac1[3:4, :, 1]
            pbipny2[bi, :, :] = jac1[3:4, :, 2]
            pbipnz2[bi, :, :] = jac1[3:4, :, 3]
        end
        #funcforward2 = (n) -> bmat_IsingADTX(ehkslice, hsxi, Ui, n[1], n[2], n[3])
        #jacxit = jacobian(funcforward2, [nxi, nyi, nzi])
        #jacxit = reshape(jacxit, 4*Nt, 2*Nx, 3)
        #pbrpnx2[:, 1, :] = jacxit[1:Nt, :, 1]
        #pbrpnx2[:, 2, :] = jacxit[(Nt+1):2Nt, :, 1]
        #pbrpny2[:, 1, :] = jacxit[1:Nt, :, 2]
        #pbrpny2[:, 2, :] = jacxit[(Nt+1):2Nt, :, 2]
        #pbrpnz2[:, 1, :] = jacxit[1:Nt, :, 3]
        #pbrpnz2[:, 2, :] = jacxit[(Nt+1):2Nt, :, 3]
        ##
        #pbipnx2[:, 1, :] = jacxit[(2Nt+1):3Nt, :, 1]
        #pbipnx2[:, 2, :] = jacxit[(3Nt+1):4Nt, :, 1]
        #pbipny2[:, 1, :] = jacxit[(2Nt+1):3Nt, :, 2]
        #pbipny2[:, 2, :] = jacxit[(3Nt+1):4Nt, :, 2]
        #pbipnz2[:, 1, :] = jacxit[(2Nt+1):3Nt, :, 3]
        #pbipnz2[:, 2, :] = jacxit[(3Nt+1):4Nt, :, 3]
    end
    return pbrpnx2, pbipnx2, pbrpny2, pbipny2, pbrpnz2, pbipnz2
end


"""
计算B矩阵的正向导数
"""
function pbpn_calc(sp, hscfg::Matrix{Int}, nx::Vector{Float64}, ny::Vector{Float64}, nz::Vector{Float64};
    tapemap=missing)
    Nt, Nx = size(hscfg)
    #
    pbrpnx2 = zeros(Nt, 2*Nx, 2*Nx, Nx)
    pbipnx2 = zeros(Nt, 2*Nx, 2*Nx, Nx)
    pbrpny2 = zeros(Nt, 2*Nx, 2*Nx, Nx)
    pbipny2 = zeros(Nt, 2*Nx, 2*Nx, Nx)
    pbrpnz2 = zeros(Nt, 2*Nx, 2*Nx, Nx)
    pbipnz2 = zeros(Nt, 2*Nx, 2*Nx, Nx)
    for xi in Base.OneTo(Nx)
        rpnx, ipnx, rpny, ipny, rpnz, ipnz = pbpn_calc_xi(
            sp, hscfg[:, xi], xi, nx[xi], ny[xi], nz[xi];
            tapemap=tapemap
        )
        pbrpnx2[:, [xi, xi+Nx], :, xi] = rpnx
        pbipnx2[:, [xi, xi+Nx], :, xi] = ipnx
        pbrpny2[:, [xi, xi+Nx], :, xi] = rpny
        pbipny2[:, [xi, xi+Nx], :, xi] = ipny
        pbrpnz2[:, [xi, xi+Nx], :, xi] = rpnz
        pbipnz2[:, [xi, xi+Nx], :, xi] = ipnz
    end
    return pbrpnx2, pbipnx2, pbrpny2, pbipny2, pbrpnz2, pbipnz2
end


"""
计算∂L/∂n
"""
function meas_grad(ss::ScrollSVD{T}, allbmats, sp, allcfgs, nx, ny, nz;
    tapemap=missing) where T
    Nt = length(allbmats)
    Nx = length(nx)
    # 计算 \bar{G} = (1 + B^-1)^-1
    # 计算 \bar{B_l}_{xy} = -(GB)^T
    bbar = bbar_calc(ss, allbmats)
    # 计算 \bar{n{xyz}_i} = \sum \bar{B_l}_xy  ∂B / ∂U^{xyz}_i 
    # 计算 \bar{n{xyz}_i} = \sum \bar{B_l}_xy  ∂B / ∂U^{xyz}_i 
    # 计算 \bar{theta_i} = \sum \bar{B_l}_xy  ∂B / ∂theta_i
    nxbar2 = zeros(Float64, Nx)
    nybar2 = zeros(Float64, Nx)
    nzbar2 = zeros(Float64, Nx)
    for bi in Base.OneTo(Nt)
        ehk2, Ui = unpack_splitting(sp, bi)
        for xi in Base.OneTo(Nx)
            ehkslice = ehk2[[xi, xi+Nx], :]
            rpnx, ipnx, rpny, ipny, rpnz, ipnz = pbpn_calc_xi(
                sp, allcfgs[:, xi], xi, nx[xi], ny[xi], nz[xi];
                tapemap=tapemap
            )
            #注意虚部附加的负号
            #xi只会和xi，xi+Nx能向前传递
            nzbar2[xi] += sum(rpnz[bi, 1, :] .* real(bbar[bi, xi, :]))
            nzbar2[xi] += sum(rpnz[bi, 2, :] .* real(bbar[bi, xi+Nx, :]))
            nzbar2[xi] -= sum(ipnz[bi, 1, :] .* imag(bbar[bi, xi, :]))
            nzbar2[xi] -= sum(ipnz[bi, 2, :] .* imag(bbar[bi, xi+Nx, :]))
            nybar2[xi] += sum(rpny[bi, 1, :] .* real(bbar[bi, xi, :]))
            nybar2[xi] += sum(rpny[bi, 2, :] .* real(bbar[bi, xi+Nx, :]))
            nybar2[xi] -= sum(ipny[bi, 1, :] .* imag(bbar[bi, xi, :]))
            nybar2[xi] -= sum(ipny[bi, 2, :] .* imag(bbar[bi, xi+Nx, :]))
            nxbar2[xi] += sum(rpnx[bi, 1, :] .* real(bbar[bi, xi, :]))
            nxbar2[xi] += sum(rpnx[bi, 2, :] .* real(bbar[bi, xi+Nx, :]))
            nxbar2[xi] -= sum(ipnx[bi, 1, :] .* imag(bbar[bi, xi, :]))
            nxbar2[xi] -= sum(ipnx[bi, 2, :] .* imag(bbar[bi, xi+Nx, :]))
        end
    end
    return nxbar2, nybar2, nzbar2
end


"""
计算每个格点上的∂B(τ)/∂a
"""
function pbpa_calc_tapemap(sp::Splitting, rax::Vector{Float64}, iax::Vector{Float64})
    #
    Nx = length(rax)
    tapemap = Matrix{AutoJacTape}(undef, Nx, 2*sp.segl)
    for si in Base.OneTo(sp.segl)
        ehk2 = reshape(sp.ehkt[si, :], sp.sizehk)
        Ui = sp.Uit[si, :]
        for xi in Base.OneTo(Nx)
            ehkslice = ehk2[[xi, xi+Nx], :]
            if abs(Ui[xi]) > 1e-6
                funcforward1 = (n) -> bmat_Gauge1ADX(ehkslice, Val(-1), Ui[xi], n[1], n[2], sp.dt)
                funcforward2 = (n) -> bmat_Gauge1ADX(ehkslice, Val(1), Ui[xi], n[1], n[2], sp.dt)
                jacxi1 = jacobian(funcforward1, [rax[xi], iax[xi]])
                jacxi2 = jacobian(funcforward2, [rax[xi], iax[xi]])
            else
                jacxi1 = zeros(Float64, 4*2*Nx*2)
                jacxi2 = zeros(Float64, 4*2*Nx*2)
            end
            tapemap[xi, 2*si-1] = AutoJacTape([rax[xi], iax[xi]], jacxi1; val=-1, U=Ui[xi])
            tapemap[xi, 2*si] = AutoJacTape([rax[xi], iax[xi]], jacxi2; val=1, U=Ui[xi])
        end
    end
    return tapemap
end


"""
计算B矩阵的正向导数
"""
function pbpa_calc_xi(sp, hsxi::Vector{Int}, xidx::Int, ra::Float64, ia::Float64;
    tapemap=missing)
    #
    Nt = length(hsxi)
    Nx = length(sp.Uit[1, :])
    pbrpra2 = zeros(Nt, 2, 2*Nx)
    pbipra2 = zeros(Nt, 2, 2*Nx)
    pbrpia2 = zeros(Nt, 2, 2*Nx)
    pbipia2 = zeros(Nt, 2, 2*Nx)
    #
    if !ismissing(tapemap)
        for bi in Base.OneTo(Nt)
            #jac1 = vcat(jacxit[[bi, Nt+bi], :, :], jacxit[[2Nt+bi, 3Nt+bi], :, :])
            segl = sp.vecl[bi]
            tape = hsxi[bi] == 1 ? tapemap[xidx, 2*segl] : tapemap[xidx, 2*segl-1]
            jac1 = reshape(tape.output, 4, 2*Nx, 2)
            #B的实数部分在1:2
            pbrpra2[bi, :, :] = jac1[1:2, :, 1]
            pbrpia2[bi, :, :] = jac1[1:2, :, 2]
            #B的虚数部分在3:4
            pbipra2[bi, :, :] = jac1[3:4, :, 1]
            pbipia2[bi, :, :] = jac1[3:4, :, 2]
        end
    else
        @warn "计算jacobian"
        #Ui = sp.Uit[:, xidx]
        #Ui = sum(Ui) / length(Ui)
        for bi in Base.OneTo(Nt)
            ehk2, Ui = unpack_splitting(sp, bi)
            funcforward2 = (n) -> bmat_Gauge1ADX(ehk2[[xidx,xidx+Nx], :],
            Val(hsxi[bi]), Ui[xidx], n[1], n[2], sp.dt)
            jacxit = jacobian(funcforward2, [ra, ia])
            jac1 = reshape(jacxit, 4, 2*Nx, 2)
            pbrpra2[bi, :, :] = jac1[1:2, :, 1]
            pbrpia2[bi, :, :] = jac1[1:2, :, 2]
            pbipra2[bi, :, :] = jac1[3:4, :, 1]
            pbipia2[bi, :, :] = jac1[3:4, :, 2]
        end
    end
    return pbrpra2, pbipra2, pbrpia2, pbipia2
end


"""
计算∂L/∂a
"""
function meas_grada(ss::ScrollSVD{T}, allbmats, sp, allcfgs, rax, iax;
    tapemap=missing) where T
    Nt, Nx = size(allcfgs)
    # 计算 \bar{G} = (1 + B^-1)^-1
    # 计算 \bar{B_l}_{xy} = -(GB)^T
    bbar = bbar_calc(ss, allbmats)
    # 计算 \bar{n{xyz}_i} = \sum \bar{B_l}_xy  ∂B / ∂U^{xyz}_i 
    # 计算 \bar{n{xyz}_i} = \sum \bar{B_l}_xy  ∂B / ∂U^{xyz}_i 
    # 计算 \bar{theta_i} = \sum \bar{B_l}_xy  ∂B / ∂theta_i
    rabar2 = zeros(Float64, Nx)
    iabar2 = zeros(Float64, Nx)
    for bi in Base.OneTo(Nt)
        #ehk2, Ui = unpack_splitting(sp, bi)
        for xi in Base.OneTo(Nx)
            #ehkslice = ehk2[[xi, xi+Nx], :]
            rpra, ipra, rpia, ipia = pbpa_calc_xi(
                sp, allcfgs[:, xi], xi, rax[xi], iax[xi];
                tapemap=tapemap
            )
            #注意虚部附加的负号
            #xi只会和xi，xi+Nx能向前传递
            rabar2[xi] += sum(rpra[bi, 1, :] .* real(bbar[bi, xi, :]))
            rabar2[xi] += sum(rpra[bi, 2, :] .* real(bbar[bi, xi+Nx, :]))
            rabar2[xi] -= sum(ipra[bi, 1, :] .* imag(bbar[bi, xi, :]))
            rabar2[xi] -= sum(ipra[bi, 2, :] .* imag(bbar[bi, xi+Nx, :]))
            #
            iabar2[xi] += sum(rpia[bi, 1, :] .* real(bbar[bi, xi, :]))
            iabar2[xi] += sum(rpia[bi, 2, :] .* real(bbar[bi, xi+Nx, :]))
            iabar2[xi] -= sum(ipia[bi, 1, :] .* imag(bbar[bi, xi, :]))
            iabar2[xi] -= sum(ipia[bi, 2, :] .* imag(bbar[bi, xi+Nx, :]))
        end
    end
    return rabar2, iabar2
end


"""
B矩阵对mu的导数
"""
function pbpmu_calc_tapemap(sp::Splitting, mui::Vector{Float64})
    Nx = size(sp.Uit)[2]
    tapemap = Matrix{AutoJacTape}(undef, Nx, 2*sp.segl)
    for si in Base.OneTo(sp.segl)
        ehk2 = reshape(sp.ehkt[si, :], sp.sizehk)
        Ui = sp.Uit[si, :]
        for xi in Base.OneTo(Nx)
            #求出没有mu的时候的ehk
            #后面作用mu上去，需要在没有mu的ehk上作用，才体现的是加了mu以后B矩阵的变化，否则就是已经有mu的B又加了mu
            ehkslice = exp(sp.dt*mui[xi])*ehk2[[xi, xi+Nx], :]
            #如果认为mu出现在右侧的话，那么要修改的就是列，比较麻烦
            funcforward1 = (mu) -> bmat_IsingADX(exp(-sp.dt*mu)*ehkslice, Val(-1), Ui[xi], 0.0, 0.0, 1.0, sp.dt)
            funcforward2 = (mu) -> bmat_IsingADX(exp(-sp.dt*mu)*ehkslice, Val(1), Ui[xi], 0.0, 0.0, 1.0, sp.dt)
            jacxi1 = derivative(funcforward1, mui[xi])
            jacxi2 = derivative(funcforward2, mui[xi])
            tapemap[xi, 2*si-1] = AutoJacTape([mui[xi]], jacxi1; val=-1, mu=mui[xi])
            tapemap[xi, 2*si] = AutoJacTape([mui[xi]], jacxi2; val=1, mu=mui[xi])
        end
    end
    return tapemap
end


"""
计算B矩阵对格点i上面的mu的导数
"""
function pbpmu_calc_xi(sp::Splitting, hsxi::Vector{Int}, xidx::Int, mui::Vector{Float64}; tapemap=missing)
    #
    Nt = length(hsxi)
    Nx = length(sp.Uit[1, :])
    pbrpmu2 = zeros(Nt, 2, 2*Nx)
    pbipmu2 = zeros(Nt, 2, 2*Nx)
    #funcforward2 = (n) -> bmat_IsingADTX(ehkslice, hsxi, Ui, n[1], n[2], n[3])
    #jacxit = jacobian(funcforward2, [nxi, nyi, nzi])
    #jacxit = reshape(jacxit, 4*Nt, 2*Nx, 3)
    #
    if !ismissing(tapemap)
        for bi in Base.OneTo(Nt)
            #jac1 = vcat(jacxit[[bi, Nt+bi], :, :], jacxit[[2Nt+bi, 3Nt+bi], :, :])
            segl = sp.vecl[bi]
            tape = hsxi[bi] == 1 ? tapemap[xidx, 2*segl] : tapemap[xidx, 2*segl-1]
            jac1 = reshape(tape.output, 4, 2*Nx)
            #@assert all(isapprox.(tape.input, [nxi, nyi, nzi]))
            #@assert all(isapprox.(jac1, reshape(tape.output, 4, 2*Nx, 3)))
            pbrpmu2[bi, :, :] = jac1[1:2, :]
            pbipmu2[bi, :, :] = jac1[3:4, :]
        end
    else
        @warn "计算jacobian"
        #
        for bi in Base.OneTo(Nt)
            ehk2, Ui = unpack_splitting(sp, bi)
            ehk2 = Diagonal(exp(sp.dt*mui))*ehk2
            #因为bmat_IsingADX中不接受vector的输入，jacibian不会重新定义exp（只有exp.）
            #这里要用derivative
            funcforward2 = (mu) -> bmat_IsingADX(exp(-sp.dt*mu)*ehk2[[xidx,xidx+Nx], :],
            Val(hsxi[bi]), Ui[xidx], 0.0, 0.0, 1.0, sp.dt)
            #
            jacxit = derivative(funcforward2, mui[xidx])
            jac1 = reshape(jacxit, 4, 2*Nx)
            pbrpmu2[bi, :, :] = jac1[1:2, :]
            pbipmu2[bi, :, :] = jac1[3:4, :]
        end
    end
    return pbrpmu2, pbipmu2
end


"""
计算∂L/∂mu
"""
function meas_gradmu(ss::ScrollSVD{T}, allbmats, sp, allcfgs, mus; tapemap=missing) where T
    Nt = length(allbmats)
    Nx = size(sp.Uit)[2]
    #这里必须指定tapemap，且必须是用baress算出来的
    if ismissing(tapemap)
        throw(DomainError("tapemap"))
    end
    # 计算 \bar{G} = (1 + B^-1)^-1
    # 计算 \bar{B_l}_{xy} = -(GB)^T
    bbar = bbar_calc(ss, allbmats)
    # 计算 \bar{n{xyz}_i} = \sum \bar{B_l}_xy  ∂B / ∂U^{xyz}_i 
    # 计算 \bar{n{xyz}_i} = \sum \bar{B_l}_xy  ∂B / ∂U^{xyz}_i 
    # 计算 \bar{theta_i} = \sum \bar{B_l}_xy  ∂B / ∂theta_i
    muibar = zeros(Float64, Nx)
    for xi in Base.OneTo(Nx)
        rpmu, ipmu = pbpmu_calc_xi(
            sp, allcfgs[:, xi], xi, mus; tapemap=tapemap
        )
        for bi in Base.OneTo(Nt)
            #注意虚部附加的负号
            #xi只会和xi，xi+Nx能向前传递
            muibar[xi] += sum(rpmu[bi, 1, :] .* real(bbar[bi, xi, :]))
            muibar[xi] += sum(rpmu[bi, 2, :] .* real(bbar[bi, xi+Nx, :]))
            muibar[xi] -= sum(ipmu[bi, 1, :] .* imag(bbar[bi, xi, :]))
            muibar[xi] -= sum(ipmu[bi, 2, :] .* imag(bbar[bi, xi+Nx, :]))
        end
    end
    return muibar
end


#=
处理tp的方法1:在右侧乘exp(dt*dp)
缺点是每一个tp都会修改所有的ehk[i,:]，因为乘在右侧修改的是列
ehkslice = ehkbare*(I + offdiag)[i,:] = ehkbare[i,:] + ehkbare*offdiag[i,:]
如果offdiag链接的是l,m两个格子
ehkbare[i,l] = ehkbare[i,m]*offdiag[2,1] + ehkbare[i,l]*offdiag[1,1]
ehkbare[i,m] = ehkbare[i,l]*offdiag[1,2] + ehkbare[i,m]*offdiag[2,2]
需要做的就是在ehkslice上面加入
exp([0 dt*tp; dt*tp 0]) = [cosh(dt*tp) sinh(dt*tp); sinh(dt*tp) cosh(dt*tp)]
function run()
ehkslice = rand(2,6)
func = function (x)
arr = hcat([x+cos(x)],zeros(1,4),[x+sin(x)]) 
arr = vcat(arr, arr)
return ehkslice + arr
end 
derivative(func, 1.0)
end
=#
#=
处理tp的方法2:在左侧乘exp(dt*dp)
缺点是每一个tp要修改两行的ehk
ehkslice = ((I + offdiag)*ehkbare)[i,:] = ehkbare[i,:] + (offdiag*ehkbare)[i,:]
如果offdiag链接的是i,j两个格子
ehkbare[i,:] = offdiag[1,2]*ehkbare[j,:] + offdiag[1,1]*ehkbare[i,:]
ehkbare[j,:] = offdiag[2,1]*ehkbare[i,:] + offdiag[2,2]*ehkbare[j,:]
=#
#=
这里采用第二种方案，只计算某两个格点的tp在所有ehk上产生的效果
=#

"""
B矩阵对hopping矩阵中非对角项的导数
"""
function pbptp_calc_tapemap(sp::Splitting, offidx::Vector{Int}, offval::Vector{Float64})
    Nx = size(sp.Uit)[2]
    tapemap = Matrix{AutoJacTape}(undef, Nx, 2*sp.segl)
    for si in Base.OneTo(sp.segl)
        ehk2 = reshape(sp.ehkt[si, :], sp.sizehk)
        Ui = sp.Uit[si, :]
        for xi in Base.OneTo(Nx)
            i1 = xi
            i2 = offidx[i1]
            ehkslice1 = ehk2[[xi, xi+Nx], :]
            ehkslice2 = ehk2[[i2, i2+Nx], :]
            #左侧乘一个exp（-dt*tp）逆矩阵
            #跟mu的情况类似，需要加上tp之后的变化，而ehk中已经有了，就需要暂时先去掉tp
            ehkslice1 = cosh(sp.dt*offval[xi])*ehkslice1 + sinh(sp.dt*offval[xi])*ehkslice2
            ehkslice2 = cosh(sp.dt*offval[xi])*ehkslice2 + sinh(sp.dt*offval[xi])*ehkslice1
            #
            funcforward1 = (mu) -> bmat_IsingADX(ehkslice1*cosh(-sp.dt*mu)+ehkslice2*sinh(-sp.dt*mu), Val(-1), Ui[xi], 0.0, 0.0, 1.0, sp.dt)
            funcforward2 = (mu) -> bmat_IsingADX(ehkslice2*cosh(-sp.dt*mu)+ehkslice1*sinh(-sp.dt*mu), Val(-1), Ui[xi], 0.0, 0.0, 1.0, sp.dt)
            funcforward3 = (mu) -> bmat_IsingADX(ehkslice1*cosh(-sp.dt*mu)+ehkslice2*sinh(-sp.dt*mu), Val(1), Ui[xi], 0.0, 0.0, 1.0, sp.dt)
            funcforward4 = (mu) -> bmat_IsingADX(ehkslice2*cosh(-sp.dt*mu)+ehkslice1*sinh(-sp.dt*mu), Val(1), Ui[xi], 0.0, 0.0, 1.0, sp.dt)
            jacxi1 = derivative(funcforward1, offval[xi])
            jacxi2 = derivative(funcforward2, offval[xi])
            jacxi3 = derivative(funcforward3, offval[xi])
            jacxi4 = derivative(funcforward4, offval[xi])
            tapemap[xi, 2*si-1] = AutoJacTape([offval[xi]], vcat(jacxi1, jacxi2); val=-1, offval=offval[xi])
            tapemap[xi, 2*si] = AutoJacTape([offval[xi]], vcat(jacxi3, jacxi4); val=1, offval=offval[xi])
        end
    end
    return tapemap
end


"""
计算B矩阵对格点i上面的mu的导数
"""
function pbptp_calc_xi(sp::Splitting, hscfg::Matrix{Int}, xidx::Int, offidx::Vector{Int}, offval::Vector{Float64};
    tapemap=missing)
    #
    Nt, Nx = size(hscfg)
    pbrpmu2 = zeros(Nt, 4, 2*Nx)
    pbipmu2 = zeros(Nt, 4, 2*Nx)
    #funcforward2 = (n) -> bmat_IsingADTX(ehkslice, hsxi, Ui, n[1], n[2], n[3])
    #jacxit = jacobian(funcforward2, [nxi, nyi, nzi])
    #jacxit = reshape(jacxit, 4*Nt, 2*Nx, 3)
    #
    if !ismissing(tapemap)
        for bi in Base.OneTo(Nt)
            #jac1 = vcat(jacxit[[bi, Nt+bi], :, :], jacxit[[2Nt+bi, 3Nt+bi], :, :])
            segl = sp.vecl[bi]
            tape = hscfg[bi, xidx] == 1 ? tapemap[xidx, 2*segl] : tapemap[xidx, 2*segl-1]
            jac1 = reshape(tape.output, 8, 2*Nx)
            #@assert all(isapprox.(tape.input, [nxi, nyi, nzi]))
            #@assert all(isapprox.(jac1, reshape(tape.output, 4, 2*Nx, 3)))
            pbrpmu2[bi, 1:2, :] = jac1[1:2, :]
            pbipmu2[bi, 1:2, :] = jac1[3:4, :]
            #另外一个位置
            tape = hscfg[bi, offidx[xidx]] == 1 ? tapemap[xidx, 2*segl] : tapemap[xidx, 2*segl-1]
            jac1 = reshape(tape.output, 8, 2*Nx)
            pbrpmu2[bi, 3:4, :] = jac1[5:6, :]
            pbipmu2[bi, 3:4, :] = jac1[7:8, :]
        end
    else
        @warn "计算jacobian"
        #
        for bi in Base.OneTo(Nt)
            ehk2, Ui = unpack_splitting(sp, bi)
            i1 = xidx
            i2 = offidx[xidx]
            ehkslice1 = ehk2[[xidx, xidx+Nx], :]
            ehkslice2 = ehk2[[i2, i2+Nx], :]
            ehkslice1 = cosh(sp.dt*mu)*ehkslice1 + sinh(sp.dt*mu)*ehkslice2
            ehkslice2 = cosh(sp.dt*mu)*ehkslice2 + sinh(sp.dt*mu)*ehkslice1
            #因为bmat_IsingADX中不接受vector的输入，jacibian不会重新定义exp（只有exp.）
            #这里要用derivative
            funcforward1 = (mu) -> bmat_IsingADX(ehkslice1*cosh(-sp.dt*mu)+ehkslice2*sinh(-sp.dt*mu), 
            Val(hsxi[bi]), Ui[xi], 0.0, 0.0, 1.0, sp.dt)
            funcforward2 = (mu) -> bmat_IsingADX(ehkslice2*cosh(-sp.dt*mu)+ehkslice1*sinh(-sp.dt*mu),
            Val(hsxi[bi]), Ui[xidx], 0.0, 0.0, 1.0, sp.dt)
            #
            jacxit1 = derivative(funcforward1, offval[xidx])
            jacxit2 = derivative(funcforward2, offval[xidx])
            jac1 = reshape(jacxit1, 4, 2*Nx)
            jac2 = reshape(jacxit2, 4, 2*Nx)
            pbrpmu2[bi, 1:2, :] = jac1[1:2, :]
            pbrpmu2[bi, 3:4, :] = jac2[1:2, :]
            pbipmu2[bi, 1:2, :] = jac1[3:4, :]
            pbipmu2[bi, 3:4, :] = jac2[3:4, :]
        end
    end
    return pbrpmu2, pbipmu2
end


"""
计算∂L/∂tp
"""
function meas_gradtp(ss::ScrollSVD{T}, allbmats, sp, allcfgs, offidx, offval; tapemap=missing) where T
    Nt = length(allbmats)
    Nx = size(sp.Uit)[2]
    #这里必须指定tapemap，且必须是用baress算出来的
    if ismissing(tapemap)
        throw(DomainError("tapemap"))
    end
    # 计算 \bar{G} = (1 + B^-1)^-1
    # 计算 \bar{B_l}_{xy} = -(GB)^T
    bbar = bbar_calc(ss, allbmats)
    # 计算 \bar{n{xyz}_i} = \sum \bar{B_l}_xy  ∂B / ∂U^{xyz}_i 
    # 计算 \bar{n{xyz}_i} = \sum \bar{B_l}_xy  ∂B / ∂U^{xyz}_i 
    # 计算 \bar{theta_i} = \sum \bar{B_l}_xy  ∂B / ∂theta_i
    muibar = zeros(Float64, Nx)
    for xi in Base.OneTo(Nx)
        rpmu, ipmu = pbptp_calc_xi(
            sp, allcfgs, xi, offidx, offval; tapemap=tapemap
        )
        for bi in Base.OneTo(Nt)
            #注意虚部附加的负号
            #xi只会和xi，xi+Nx能向前传递
            muibar[xi] += sum(rpmu[bi, 1, :] .* real(bbar[bi, xi, :]))
            muibar[xi] += sum(rpmu[bi, 2, :] .* real(bbar[bi, xi+Nx, :]))
            muibar[xi] += sum(rpmu[bi, 3, :] .* real(bbar[bi, offidx[xi], :]))
            muibar[xi] += sum(rpmu[bi, 4, :] .* real(bbar[bi, offidx[xi]+Nx, :]))
            muibar[xi] -= sum(ipmu[bi, 1, :] .* imag(bbar[bi, xi, :]))
            muibar[xi] -= sum(ipmu[bi, 2, :] .* imag(bbar[bi, xi+Nx, :]))
            muibar[xi] -= sum(ipmu[bi, 3, :] .* imag(bbar[bi, offidx[xi], :]))
            muibar[xi] -= sum(ipmu[bi, 4, :] .* imag(bbar[bi, offidx[xi]+Nx, :]))
        end
    end
    return muibar
end


"""
计算每个格点上的jactape，在计算∂B(τ)/∂U_{i}的时候，所有的i之间和τ之间没有什么关联，
每个格点和虚时都单看只有辅助场s_{τ,i}=±1的两种，把他们提前算出来，然后在不同HS场下平均的时候就不要重复计算了。
"""
function pbpU_calc_tapemap(sp::Splitting)
    #
    Nx = size(sp.Uit)[2]
    tapemap = Matrix{AutoJacTape}(undef, Nx, 2*sp.segl)
    for si in Base.OneTo(sp.segl)
        ehk2 = reshape(sp.ehkt[si, :], sp.sizehk)
        Ui = sp.Uit[si, :]
        for xi in Base.OneTo(Nx)
            ehkslice = ehk2[[xi, xi+Nx], :]
            if abs(Ui[xi]) > 1e-6
                funcforward1 = (U) -> bmat_IsingADX(ehkslice, Val(-1), U, 0.0, 0.0, 1.0, sp.dt)
                funcforward2 = (U) -> bmat_IsingADX(ehkslice, Val(1), U, 0.0, 0.0, 1.0, sp.dt)
                jacxi1 = derivative(funcforward1, Ui[xi])
                jacxi2 = derivative(funcforward2, Ui[xi])
            else
                jacxi1 = zeros(Float64, 4*2*Nx)
                jacxi2 = zeros(Float64, 4*2*Nx)
            end
            tapemap[xi, 2*si-1] = AutoJacTape([Ui[xi]], jacxi1; val=-1, U=Ui[xi])
            tapemap[xi, 2*si] = AutoJacTape([Ui[xi]], jacxi2; val=1, U=Ui[xi])
        end
    end
    return tapemap
end


"""
计算B矩阵的正向导数
"""
function pbpU_calc_xi(sp::Splitting, hsxi::Vector{Int}, xidx::Int; tapemap=missing)
    #
    Nt = length(hsxi)
    Nx = length(sp.Uit[1, :])
    pbrpU2 = zeros(Nt, 2, 2*Nx)
    pbipU2 = zeros(Nt, 2, 2*Nx)
    #funcforward2 = (n) -> bmat_IsingADTX(ehkslice, hsxi, Ui, n[1], n[2], n[3])
    #jacxit = jacobian(funcforward2, [nxi, nyi, nzi])
    #jacxit = reshape(jacxit, 4*Nt, 2*Nx, 3)
    #
    if !ismissing(tapemap)
        for bi in Base.OneTo(Nt)
            #jac1 = vcat(jacxit[[bi, Nt+bi], :, :], jacxit[[2Nt+bi, 3Nt+bi], :, :])
            segl = sp.vecl[bi]
            tape = hsxi[bi] == 1 ? tapemap[xidx, 2*segl] : tapemap[xidx, 2*segl-1]
            jac1 = reshape(tape.output, 4, 2*Nx)
            #@assert all(isapprox.(tape.input, [nxi, nyi, nzi]))
            #@assert all(isapprox.(jac1, reshape(tape.output, 4, 2*Nx, 3)))
            pbrpU2[bi, :, :] = jac1[1:2, :]
            pbipU2[bi, :, :] = jac1[3:4, :]
        end
    else
        @warn "计算jacobian"
        #Ui = sp.Uit[:, xidx]
        #Ui = sum(Ui) / length(Ui)
        for bi in Base.OneTo(Nt)
            ehk2, Ui = unpack_splitting(sp, bi)
            #因为bmat_IsingADX中不接受vector的输入，jacibian不会重新定义exp（只有exp.）
            #这里要用derivative
            funcforward2 = (U) -> bmat_IsingADX(ehk2[[xidx,xidx+Nx], :],
            Val(hsxi[bi]), U, 0.0, 0.0, 1.0, sp.dt)
            #
            jacxit = derivative(funcforward2, Ui[xidx])
            jac1 = reshape(jacxit, 4, 2*Nx)
            pbrpU2[bi, :, :] = jac1[1:2, :]
            pbipU2[bi, :, :] = jac1[3:4, :]
        end
    end
    return pbrpU2, pbipU2
end


"""
计算lnS对U的导数
"""
function meas_gradU(ss::ScrollSVD{T}, allbmats, sp, allcfgs; tapemap=missing) where T
    Nt = length(allbmats)
    Nx = size(sp.Uit)[2]
    # 计算 \bar{G} = (1 + B^-1)^-1
    # 计算 \bar{B_l}_{xy} = -(GB)^T
    bbar = bbar_calc(ss, allbmats)
    # 计算 \bar{n{xyz}_i} = \sum \bar{B_l}_xy  ∂B / ∂U^{xyz}_i 
    # 计算 \bar{n{xyz}_i} = \sum \bar{B_l}_xy  ∂B / ∂U^{xyz}_i 
    # 计算 \bar{theta_i} = \sum \bar{B_l}_xy  ∂B / ∂theta_i
    Uibar2 = zeros(Float64, Nx)
    for xi in Base.OneTo(Nx)
        rpU, ipU = pbpU_calc_xi(
            sp, allcfgs[:, xi], xi;tapemap=tapemap
        )
        for bi in Base.OneTo(Nt)
            #注意虚部附加的负号
            #xi只会和xi，xi+Nx能向前传递
            Uibar2[xi] += sum(rpU[bi, 1, :] .* real(bbar[bi, xi, :]))
            Uibar2[xi] += sum(rpU[bi, 2, :] .* real(bbar[bi, xi+Nx, :]))
            Uibar2[xi] -= sum(ipU[bi, 1, :] .* imag(bbar[bi, xi, :]))
            Uibar2[xi] -= sum(ipU[bi, 2, :] .* imag(bbar[bi, xi+Nx, :]))
        end
    end
    return Uibar2
end

#=
"""
计算∂L/∂n
"""
function meas_grad_(ss::ScrollSVD{T}, allbmats, hk, allcfgs, Ui, nx, ny, nz; Z2=true) where T
    Nt = length(allbmats)
    Nx = length(Ui)
    # 计算 \bar{G} = (1 + B^-1)^-1
    # 计算 \bar{B_l}_{xy} = -(GB)^T
    bbar = bbar_calc(ss, allbmats)
    # 计算 \bar{n{xyz}_i} = \sum \bar{B_l}_xy  ∂B / ∂U^{xyz}_i 
    Uval = sum(Ui) / length(Ui)
    pbrpnx, pbipnx, pbrpny, pbipny, pbrpnz, pbipnz = pbpn_calc(hk, allcfgs, Uval, nx, ny, nz; Z2=Z2)
    dt = 0.1
    ehk = exp(-dt*hk)
    if Z2
        ehk2 = kron([1 0; 0 1], ehk)
    else
        ehk2 = ehk
    end
    # 计算 \bar{n{xyz}_i} = \sum \bar{B_l}_xy  ∂B / ∂U^{xyz}_i 
    # 计算 \bar{theta_i} = \sum \bar{B_l}_xy  ∂B / ∂theta_i
    nxbar2 = zeros(Float64, Nx)
    nybar2 = zeros(Float64, Nx)
    nzbar2 = zeros(Float64, Nx)
    for xi in Base.OneTo(Nx)
        ehkslice = ehk2[[xi, xi+Nx], :]
        rpnx, ipnx, rpny, ipny, rpnz, ipnz = pbpn_calc_xi(
            ehkslice, allcfgs[:, xi], Ui[xi], nx[xi], ny[xi], nz[xi]
        )
        #@assert all(isapprox.(pbrpnx[:, [xi, xi+Nx], :, xi], rpnx))
        #@assert all(isapprox.(pbipnx[:, [xi, xi+Nx], :, xi], ipnx))
        #@assert all(isapprox.(pbrpny[:, [xi, xi+Nx], :, xi], rpny))
        #@assert all(isapprox.(pbipny[:, [xi, xi+Nx], :, xi], ipny))
        #@assert all(isapprox.(pbrpnz[:, [xi, xi+Nx], :, xi], rpnz))
        #@assert all(isapprox.(pbipnz[:, [xi, xi+Nx], :, xi], ipnz))
        #xi只会和xi，xi+Nx能向前传递
        nzbar2[xi] += sum(rpnz[:, 1, :] .* real(bbar[:, xi, :]))
        nzbar2[xi] += sum(rpnz[:, 2, :] .* real(bbar[:, xi+Nx, :]))
        nzbar2[xi] -= sum(ipnz[:, 1, :] .* imag(bbar[:, xi, :]))
        nzbar2[xi] -= sum(ipnz[:, 2, :] .* imag(bbar[:, xi+Nx, :]))
        nybar2[xi] += sum(rpny[:, 1, :] .* real(bbar[:, xi, :]))
        nybar2[xi] += sum(rpny[:, 2, :] .* real(bbar[:, xi+Nx, :]))
        nybar2[xi] -= sum(ipny[:, 1, :] .* imag(bbar[:, xi, :]))
        nybar2[xi] -= sum(ipny[:, 2, :] .* imag(bbar[:, xi+Nx, :]))
        nxbar2[xi] += sum(rpnx[:, 1, :] .* real(bbar[:, xi, :]))
        nxbar2[xi] += sum(rpnx[:, 2, :] .* real(bbar[:, xi+Nx, :]))
        nxbar2[xi] -= sum(ipnx[:, 1, :] .* imag(bbar[:, xi, :]))
        nxbar2[xi] -= sum(ipnx[:, 2, :] .* imag(bbar[:, xi+Nx, :]))
    end
    #
    nxbar = zeros(Float64, Nx)
    nybar = zeros(Float64, Nx)
    nzbar = zeros(Float64, Nx)
    #注意虚部附加的负号
    #TODO：这里先算出来整个的pbrpnz实在太消耗内存了，优化成计算某个时刻和位置的
    for bi in Base.OneTo(Nt); for xi in Base.OneTo(2*Nx); for yi in Base.OneTo(2*Nx)
        nzbar += pbrpnz[bi, xi, yi, :] * real(bbar[bi, xi, yi])
        nzbar -= pbipnz[bi, xi, yi, :] * imag(bbar[bi, xi, yi])
        nybar += pbrpny[bi, xi, yi, :] * real(bbar[bi, xi, yi])
        nybar -= pbipny[bi, xi, yi, :] * imag(bbar[bi, xi, yi])
        nxbar += pbrpnx[bi, xi, yi, :] * real(bbar[bi, xi, yi])
        nxbar -= pbipnx[bi, xi, yi, :] * imag(bbar[bi, xi, yi])
    end; end; end
    @assert all(isapprox.(nxbar, nxbar2))
    @assert all(isapprox.(nybar, nybar2))
    @assert all(isapprox.(nzbar, nzbar2))
    return nxbar2, nybar2, nzbar2
end
=#
