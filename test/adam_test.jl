
include("../src/qmcmary.jl")
using ..qmcmary

using Random

function run()
    f(x, y) = x^2 + (y-1)^2
    fpx(x) = 2x
    fpy(y) = 2(y-1)
    #
    x0, y0 = rand(), rand()
    xa, ya = x0, y0
    xs, ys = x0, y0
    t = 0
    m = zeros(2)
    v = zeros(2)
    for ti in Base.OneTo(100)
        val = f(xa, ya)
        dvdx = [fpx(xa), fpy(ya)]
        lg, m, v, t = next(Adam, dvdx, m, v, t)
        println("val $(val) dvdx $(dvdx) lg $(lg) m $(m) v $(v) t $(t)")
        xa -= lg[1]
        ya -= lg[2]
        val = f(xs, ys)
        dvdx = [fpx(xs), fpy(ys)]
        println("val $(val) dvdx $(dvdx)")
        xs -= 0.001dvdx[1]
        ys -= 0.001dvdx[2]
    end
    #
end



run()




