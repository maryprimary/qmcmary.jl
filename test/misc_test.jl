
include("../src/qmcmary.jl")
using ..qmcmary

using Test
using ReverseDiff: jacobian, jacobian!
using LinearAlgebra

BLAS.set_num_threads(1)


function misc()
    c = 1.0
    d = 10.0
    @← c d
end
misc()