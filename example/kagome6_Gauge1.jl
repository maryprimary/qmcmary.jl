#=
/opt/openmpi/bin/mpiexec
=#

#在openmpi中运行openblas中的函数时经常有问题，是不是因为我用的是ifort编译的openmpi？
#改用intelmpi或者用MKL
#在利用MPI的情况下，BLAS的多线程实现会很大的干扰性能，用intelmpi+MKL SEQUENTIAL
#MKL的引用放在最上面
using MKL
MKL.set_threading_layer(MKL.THREADING_SEQUENTIAL)

include("../src/qmcmary.jl")
using ..qmcmary

using Random
using MPI
using Dates


"""保存hs场的位型"""
function savehs(hscfg, head, fname)
    fil = open(fname, "a")
    write(fil, head)
    write(fil, "\n")
    Nt = size(hscfg)[1]
    for ni in Base.OneTo(Nt)
        write(fil, string(hscfg[ni, :]))
        write(fil, "\n")
    end
    close(fil)
end


"""
进行前向计算
"""
function forward(sp, Nt, Nx, Ng, rax, iax; hsf=nothing)
    comm = MPI.COMM_WORLD
    fname = "tmp$(MPI.Comm_rank(comm))_$(MPI.Comm_size(comm))"
    fil = open(fname, "w")
    close(fil)
    hscfg::Matrix{Int} = Matrix{Int}(undef, Nt, Nx)
    nwarm = 0
    if isnothing(hsf)
        for bi in Base.OneTo(Nt)
            cfg = 2(round.(rand(Nx)) .- 0.5)
            hscfg[bi, :] .= cfg
        end
        nwarm = 10
    else
        hscfg = hsf
    end
    sslen, bmats, allbmats, ss = initialize_SS_Gauge1(
        Nt, Ng, sp, hscfg, rax, iax
    )
    println("rank: $(MPI.Comm_rank(comm))_$(MPI.Comm_size(comm)) tick1: $(Dates.now())")
    for _ in Base.OneTo(nwarm)
        hscfg, bmats, allbmats, ss, gf, ph = dqmc_step_Gauge1(
            Nt, Ng, sp, hscfg, rax, iax, sslen, bmats, allbmats, ss
        )
    end
    println("rank: $(MPI.Comm_rank(comm))_$(MPI.Comm_size(comm)) tick2: $(Dates.now())")
    #
    tph = 0.0
    nsp = 10
    for idx in Base.OneTo(nsp)
        hscfg, bmats, allbmats, ss, gf, ph = dqmc_step_Gauge1(
            Nt, Ng, sp, hscfg, rax, iax, sslen, bmats, allbmats, ss
        )
        tph += ph
        savehs(hscfg, string(idx), fname)
    end
    println("rank: $(MPI.Comm_rank(comm))_$(MPI.Comm_size(comm)) tick3: $(Dates.now())")
    return tph/nsp, hscfg
    #println("forward sgn: ", tph/nsp)
end


"""
导数
"""
function step_theta1(sp, Nt, Nx, Ng, rax, iax)
    comm = MPI.COMM_WORLD
    fid = open("tmp$(MPI.Comm_rank(comm))_$(MPI.Comm_size(comm))", "r")
    #
    #重新读取cfg
    raxbar = zeros(Nx)
    iaxbar = zeros(Nx)
    tph = 0.0
    bistr = "0"
    tapemap = pbpa_calc_tapemap(sp, rax, iax)
    println("rank: $(MPI.Comm_rank(comm))_$(MPI.Comm_size(comm)) tick4: $(Dates.now())")
    while !eof(fid)
        bistr = readline(fid)
        #println(bistr)
        hscfg = Matrix{Int}(undef, Nt, Nx)
        for bi in Base.OneTo(Nt)
            cfg = readline(fid)
            cfg = split(cfg[2:end-1], ',')
            for xi in Base.OneTo(Nx)
                hscfg[bi, xi] = strip(cfg[xi]) == "1" ? +1 : -1
            end
        end
        sslen, bmats, allbmats, ss = initialize_SS_Gauge1(
            Nt, Ng, sp, hscfg, rax, iax
        )
        ###
        gf, ph = eq_green_scratch(ss)
        #println(ph)
        tph += ph
        #
        drax, diax = meas_grada(
            ss, allbmats, sp, hscfg, rax, iax; tapemap=tapemap
        )
        #println("drax: ", drax)
        #println("diax: ", diax)
        raxbar += drax
        iaxbar += diax
    end
    println("rank: $(MPI.Comm_rank(comm))_$(MPI.Comm_size(comm)) tick5: $(Dates.now())")
    close(fid)
    nsp = parse(Int, bistr)
    #println(tph/nsp)
    return raxbar/nsp, iaxbar/nsp
end



function optimal(L)
    MPI.Init()
    #Nx = 3*L^2
    #hk = lattice_kagome(ComplexF64, L, -1.0+0.0im)
    Nx = L^2
    tp = 0.25
    hk = lattice_tprim_square(ComplexF64, L, -1.0+0.0im, tp+0.0im)
    Ui = 6*ones(Nx)
    Nt = 60
    Ng = 4
    dt = 0.1
    rax = @. 2acosh(exp(Ui*dt/2))/Ui/dt
    iax = zeros(Nx)
    #
    comm = MPI.COMM_WORLD
    t_rax = 0
    m_rax = zeros(Nx)
    v_rax = zeros(Nx)
    t_iax = 0
    m_iax = zeros(Nx)
    v_iax = zeros(Nx)
    hsfig = nothing
    #
    sp = default_splitting(Nt, hk, Ui; Z2=true, dt=dt)
    for st in Base.OneTo(100)
        sgn, hsfig = forward(sp, Nt, Nx, Ng, rax, iax; hsf=hsfig)
        raxbar, iaxbar = step_theta1(sp, Nt, Nx, Ng, rax, iax)
        #Reduce会自动等执行完成, 用Allduce效果是一样的
        println("sgn: $(sgn) $(MPI.Comm_rank(comm))")
        sum = MPI.Reduce(sgn, +, 0, comm)
        sum = MPI.bcast(sum, 0, comm)
        sum = sum / MPI.Comm_size(comm)
        #
        raxbarsum = MPI.Reduce(raxbar, +, 0, comm)
        iaxbarsum = MPI.Reduce(iaxbar, +, 0, comm)
        if MPI.Comm_rank(comm) == 0
            raxbarsum = raxbarsum / MPI.Comm_size(comm)
            iaxbarsum = iaxbarsum / MPI.Comm_size(comm)
            println("forward sign: $(sum)")
            #println("t: $(tbarsum)")
            #println("p: $(pbarsum)")
            lg_rax, m_rax, v_rax, t_rax = next(Adam, raxbarsum, m_rax, v_rax, t_rax; α=0.03)
            lg_iax, m_iax, v_iax, t_iax = next(Adam, iaxbarsum, m_iax, v_iax, t_iax; α=0.03)
            rax = rax .+ lg_rax
            iax = iax .+ lg_iax
        end
        rax = MPI.bcast(rax, 0, comm)
        iax = MPI.bcast(iax, 0, comm)
        #println("$(MPI.Comm_rank(comm)) mt: $(m_the) vt: $(v_the)")
        #println("$(MPI.Comm_rank(comm)) mt: $(m_psi) vt: $(v_psi)")
        println("$(MPI.Comm_rank(comm)) rax: $(rax) iax: $(iax)")
        #println("$(MPI.Comm_rank(comm)) hscfg: $(hsfig[1:3, :])")
    end
end

optimal(6)
