#=
/opt/openmpi/bin/mpiexec
=#

#在openmpi中运行openblas中的函数时经常有问题，是不是因为我用的是ifort编译的openmpi？
#改用intelmpi或者用MKL
#在利用MPI的情况下，BLAS的多线程实现会很大的干扰性能，用intelmpi+MKL SEQUENTIAL
#MKL的引用放在最上面
using MKL
MKL.set_threading_layer(MKL.THREADING_SEQUENTIAL)

include("../src/qmcmary.jl")
using ..qmcmary

using LinearAlgebra
using Random
using MPI
using Dates



"""
进行前向计算
"""
function forward(sp, muv, Nt, Nx, Ng, psi, the; hsf=missing)
    nx = sin.(psi).*sin.(the)
    ny = cos.(psi).*sin.(the)
    nz = cos.(the)
    comm = MPI.COMM_WORLD
    fname = "tmp$(MPI.Comm_rank(comm))_$(MPI.Comm_size(comm))"
    fil = open(fname, "w")
    close(fil)
    hscfg::Matrix{Int} = Matrix{Int}(undef, Nt, Nx)
    nwarm = 0
    if ismissing(hsf)
        for bi in Base.OneTo(Nt)
            cfg = 2(round.(rand(Nx)) .- 0.5)
            hscfg[bi, :] .= cfg
        end
        nwarm = 100
    else
        hscfg = hsf
    end
    sslen, bmats, allbmats, ss = initialize_SS(
        Nt, Ng, sp, hscfg, nx, ny, nz
    )
    println("rank: $(MPI.Comm_rank(comm))_$(MPI.Comm_size(comm)) tick1: $(Dates.now())")
    for _ in Base.OneTo(nwarm)
        hscfg, bmats, allbmats, ss, gf, ph = dqmc_step(
            Nt, Ng, sp, hscfg, nx, ny, nz, sslen, bmats, allbmats, ss
        )
    end
    println("rank: $(MPI.Comm_rank(comm))_$(MPI.Comm_size(comm)) tick2: $(Dates.now())")
    #
    tph = 0.0
    aden = 0.0
    sdmu = 0.0
    nsp = 100
    tapemap = pbpmu_calc_tapemap(sp, muv)
    for idx in Base.OneTo(nsp)
        hscfg, bmats, allbmats, ss, gf, ph = dqmc_step(
            Nt, Ng, sp, hscfg, nx, ny, nz, sslen, bmats, allbmats, ss
        )
        tph += ph
        mubar = meas_gradmu(ss, allbmats, sp, hscfg, muv; tapemap=tapemap)
        for xi in Base.OneTo(Nx)
            ##这里到底是对Delta求导还是对 Delta_i = ± Delta 求导？
            #meas_gradmu是对Delta_i的求导，这样的话，aden也应该要对Delta_i求导，于是不应该是
            #交错的密度
            #meas_gradmu是对Delta_i的求导(∂lnS/∂mui)，而Delta_i = ± Delta
            #∂mui/∂Δ ∂lnS/∂mui = sign(mui) ∂lnS/∂mui
            aden += sign(muv[xi])*ph*(2.0 - gf[xi, xi] - gf[xi+Nx, xi+Nx])/Nx
            #这部分没有符号问题
            sdmu += sign(muv[xi])*mubar[xi]/Nx
        end
    end
    println("rank: $(MPI.Comm_rank(comm))_$(MPI.Comm_size(comm)) tick3: $(Dates.now())")
    #
    tph = tph/nsp
    aden = aden/nsp/tph
    #这部分没有符号问题，不做reweight
    sdmu = sdmu/nsp
    res = -Nt*sp.dt*aden + sdmu
    return tph, aden, sdmu, res, hscfg
end


function sgn(L)
    MPI.Init()
    Nx = L^2
    delta = 0.1
    hk = lattice_ionic_square(ComplexF64, L, -1.0+0.0im, delta+0.0im)
    #
    muv = real(diag(lattice_ionic_square(ComplexF64, L, 0.0+0.0im, delta+0.0im)))
    #println(hk)
    #println(muv, length(muv))
    Ui = 1.0*ones(Nx)
    Nt = 200
    Ng = 4
    psi = zeros(Nx)
    the = zeros(Nx)
    #
    comm = MPI.COMM_WORLD
    #
    sp = default_splitting(Nt, hk, Ui; Z2=true)
    hsfig = missing
    #
    nbin = 10
    sgnbin = zeros(ComplexF64, MPI.Comm_size(comm), nbin)
    adenbin = zeros(ComplexF64, MPI.Comm_size(comm), nbin)
    sdmubin = zeros(ComplexF64, MPI.Comm_size(comm), nbin)
    resbin = zeros(ComplexF64, MPI.Comm_size(comm), nbin)
    for binidx in Base.OneTo(nbin)
        sgn, aden, sdmu, res, hsfig = forward(sp, muv, Nt, Nx, Ng, psi, the; hsf=hsfig)
        sgnbin[MPI.Comm_rank(comm)+1, binidx] = sgn
        adenbin[MPI.Comm_rank(comm)+1, binidx] = aden
        sdmubin[MPI.Comm_rank(comm)+1, binidx] = sdmu
        resbin[MPI.Comm_rank(comm)+1, binidx] = res
    end
    #MPI.Barrier()
    #println("$(MPI.Comm_rank(comm))", sgnbin)
    sgnbinall = MPI.Reduce(sgnbin, +, 0, comm)
    adenbinall = MPI.Reduce(adenbin, +, 0, comm)
    sdmubinall = MPI.Reduce(sdmubin, +, 0, comm)
    resbinall = MPI.Reduce(resbin, +, 0, comm)
    #println("$(MPI.Comm_rank(comm))", sgnbinall)
    if MPI.Comm_rank(comm) == 0
        totb = nbin * MPI.Comm_size(comm)
        avgsgn = sum(sgnbinall)/totb
        errsgn = sqrt(sum((sgnbinall .- avgsgn).^2)/(totb-1))
        println("sgn: $(avgsgn) +- $(errsgn)")
        avgaden = sum(adenbinall)/totb
        erraden = sqrt(sum((adenbinall .- avgaden).^2)/(totb-1))
        println("den: $(avgaden) +- $(erraden)")
        avgsdmu = sum(sdmubinall)/totb
        errsdmu = sqrt(sum((sdmubinall .- avgsdmu).^2)/(totb-1))
        println("sdu: $(avgsdmu) +- $(errsdmu)")
        avgres = sum(resbinall)/totb
        errres = sqrt(sum((resbinall .- avgres).^2)/(totb-1))
        println("res: $(avgres) +- $(errres)")
    end
end

sgn(6)
